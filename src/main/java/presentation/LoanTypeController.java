package presentation;

import dataAccess.CRUD;
import dataAccess.entities.GrantConditionEntity;
import domainLogic.LoanTypeLogic;
import domainLogic.domainObjects.LoanTypeObject;
import exceptions.BusinessException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class LoanTypeController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String pageToForward="";
        try {
            String loanName = request.getParameter("loanName");
            Float interestRate = Float.parseFloat(request.getParameter("interestRate"));

            // load proper loan type
            LoanTypeObject loanTypeObject = LoanTypeLogic.create(loanName, interestRate);
            List<GrantConditionEntity> grantConditionEntities = CRUD.loadByNameAndPercent(loanName,interestRate);
            request.setAttribute("grantConditionEntities",grantConditionEntities);
            request.setAttribute("loanTypeObject", loanTypeObject);
            pageToForward="/create-grant-condition.jsp";
        } catch ( BusinessException e) {
            request.setAttribute("header","عملیات ناموفق");
            request.setAttribute("text","خطا در ثبت نوع تسهیلات جدید! " + "\n" + e.getMessage());
            request.setAttribute("url","create-loan-type.jsp");
            pageToForward="/info-page.jsp";
        }finally {
            try {
                getServletConfig().getServletContext().getRequestDispatcher(pageToForward).forward(request,response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
