package presentation;

import dataAccess.CRUD;
import domainLogic.GrantConditionLogic;
import domainLogic.domainObjects.GrantConditionObject;
import domainLogic.domainObjects.LoanTypeObject;
import exceptions.BusinessException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GrantConditionController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String loanName = request.getParameter("loanName");
            Float interestRate = Float.parseFloat(request.getParameter("interestRate"));

            int rowCount = Integer.parseInt(request.getParameter("rowCount"));
            String deletedId = request.getParameter("delete_id");
            List<Integer> deleteIdIntegers = new LinkedList<>();
            if (deletedId != null && deletedId.length() > 0) {
                deletedId = deletedId.trim();
                deletedId = deletedId.substring(1);
                String[] ids = deletedId.split(",");
                System.out.println(ids.length);
                for (String id : ids) {
                    if (id.length() > 0) {
                        deleteIdIntegers.add(Integer.valueOf(id));
                    }
                }
            }

            for (Integer integer : deleteIdIntegers) {
                CRUD.deleteGrant(integer);
            }
            ArrayList<GrantConditionObject> grantConditions = new ArrayList<GrantConditionObject>();
            for (int i = 1; i < rowCount ; i++) {
                GrantConditionObject grantConditionObject = new GrantConditionObject();
                String grantName = request.getParameter("conditionName" + i);
                if (grantName != null) {
                    grantConditionObject.setGrantName(grantName);
                    grantConditionObject.setMinDuration(Integer.parseInt((request.getParameter("minDuration" + i))));
                    grantConditionObject.setMaxDuration(Integer.parseInt((request.getParameter("maxDuration" + i))));
                    grantConditionObject.setMinAmount(new BigDecimal((request.getParameter("minAmount" + i))));
                    grantConditionObject.setMaxAmount(new BigDecimal((request.getParameter("maxAmount" + i))));
                    grantConditions.add(grantConditionObject);
                }
            }
            GrantConditionLogic.create(new LoanTypeObject(request.getParameter("loanName"), Float.parseFloat(request.getParameter("interestRate"))), grantConditions);

            request.setAttribute("header", "عملیات موفق");
            request.setAttribute("text", "نوع تسهیلات جدید با موفقیت ثبت شد!");
            request.setAttribute("url", "create-loan-type.jsp");
        } catch (BusinessException e) {
            request.setAttribute("header", "عملیات ناموفق");
            request.setAttribute("text", " خطا در ذخیره نوع تسهیلات. لطفا مجددا تلاش کنید!" + "\n" + e.getMessage());
            request.setAttribute("url", "create-loan-type.jsp");
        }
        getServletConfig().getServletContext().getRequestDispatcher("/info-page.jsp").forward(request, response);


    }
}
