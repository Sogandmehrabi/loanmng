import domainLogic.LoanFileLogic;
import domainLogic.domainObjects.LoanFileObject;
import exceptions.BusinessException;

import java.math.BigDecimal;


public class TestApplication {
    public static void main(String[] args) {



        try {
            LoanFileObject loanFileObject =  new LoanFileObject();
            loanFileObject.setAmount(new BigDecimal(5));
            loanFileObject.setDuration(5);
            LoanFileLogic.validateLoanFile(loanFileObject,48);
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }
}
