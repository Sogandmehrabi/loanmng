package domainLogic;

import dataAccess.CRUD;
import domainLogic.domainObjects.GrantConditionObject;
import domainLogic.domainObjects.LoanFileObject;
import domainLogic.domainObjects.LoanTypeObject;
import domainLogic.domainObjects.RealCustomerObject;
import exceptions.BusinessException;

import java.math.BigDecimal;
import java.util.ArrayList;

public class LoanFileLogic {

    public static void validateLoanFile(LoanFileObject loanFileObject, Integer loanId)
            throws BusinessException {

        ArrayList<GrantConditionObject> grantConditionObjects = GrantConditionLogic.retrieveConditionsByLoanId(loanId);
        for (GrantConditionObject grantConditionObject : grantConditionObjects) {
            if (loanFileObject.getDuration() > grantConditionObject.getMaxDuration() || loanFileObject.getDuration() < grantConditionObject.getMinDuration())
                throw new BusinessException("مدت زمان وارد شده در محدوده مدت زمان های شرایط تسهیلات صدق نمی کند! لطفا دوباره تلاش کنید.");

            if (loanFileObject.getAmount().compareTo(grantConditionObject.getMaxAmount()) > 0 || loanFileObject.getAmount().compareTo(new BigDecimal(grantConditionObject.getMinDuration())) < 0)
                throw new BusinessException("مبلغ وارد شده در محدوده مبلغ های شرایط تسهیلات صدق نمی کند! لطفا دوباره تلاش کنید.");

        }
    }

    public static RealCustomerObject retrieveCustomer(Integer customerId)
            throws BusinessException {

        return RealCustomerLogic.retrieve(customerId);
    }

    public static ArrayList<LoanTypeObject> retrieveLoanTypes()
            throws BusinessException {

        return LoanTypeLogic.retrieveAll();
    }

    public static LoanTypeObject retrieveLoanType(Integer loanTypeId)
            throws BusinessException {

        return LoanTypeLogic.retrieve(loanTypeId);
    }

    public static void create(Integer customerId, Integer loanTypeId, LoanFileObject loanFileObject)
            throws BusinessException {

        try {
            LoanTypeObject loanTypeObject = retrieveLoanType(loanTypeId);
            validateLoanFile(loanFileObject, loanTypeId);
            loanFileObject.setLoanType(loanTypeObject);
            RealCustomerObject realCustomerObject = retrieveCustomer(customerId);
            loanFileObject.setRealCustomer(realCustomerObject);
            CRUD.saveLoanFile(loanFileObject.toLoanFileEntity(), loanTypeObject.toLoanTypeEntity(), realCustomerObject.toRealCustomerEntity());
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }
}
