package domainLogic;

import dataAccess.CRUD;
import dataAccess.entities.GrantConditionEntity;
import domainLogic.domainObjects.GrantConditionObject;
import domainLogic.domainObjects.LoanTypeObject;
import exceptions.BusinessException;

import java.util.ArrayList;
import java.util.List;

public class GrantConditionLogic {

    public static void create(LoanTypeObject loanTypeObject, ArrayList<GrantConditionObject> grantConditions)
            throws BusinessException {

        validateGrantConditions(grantConditions);
        CRUD.saveLoanType(loanTypeObject.toLoanTypeEntity(), GrantConditionObject.toLoanTypeEntity(grantConditions));

    }

    private static void validateGrantConditions(ArrayList<GrantConditionObject> grantConditions)
            throws BusinessException {

        for(GrantConditionObject grantConditionObject : grantConditions){
            if(grantConditionObject.getMinDuration() > grantConditionObject.getMaxDuration())
                throw new BusinessException("حداکثر مدت قرارداد باید بزرگتر از حداقل مدت قرارداد باشد.");

            if(grantConditionObject.getMinAmount().compareTo(grantConditionObject.getMaxAmount())==1)
                throw new BusinessException("حداکثر مبلغ قرارداد باید بزرگتر از حداقل مدت قرارداد باشد.");

        }
    }

    public static ArrayList<GrantConditionObject> retrieveConditionsByLoanId(Integer loanId)
            throws BusinessException {

        List<GrantConditionEntity> grantConditionEntities = CRUD.retrieveLoanTypeConditions(loanId);
        ArrayList<GrantConditionObject> grantConditionObjects = new ArrayList<>();
        for(GrantConditionEntity grantConditionEntity : grantConditionEntities){
            grantConditionObjects.add(GrantConditionObject.convert(grantConditionEntity));
        }
        return  grantConditionObjects;
    }
}
