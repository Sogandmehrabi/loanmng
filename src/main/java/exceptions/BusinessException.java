package exceptions;

public class BusinessException extends Throwable {
    public BusinessException(String message) {
        super(message);
    }
}
